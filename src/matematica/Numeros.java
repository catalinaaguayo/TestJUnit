/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matematica;

/**
 *
 * @author catalinaaguayo
 */
public class Numeros {
    
    public Boolean numeroPar (int n){
        return n%2==0;
    }
    
    public int areaCuadrado (int lado){
        return lado*lado;
    }
    
    public Boolean numeroDivisible (int dividendo, int divisor){
        return dividendo%divisor==0;
    }
}
